 * chapter 10: Javascript
	 * Javascript types:
	
	   - Number
	   - String
	   -  Boolean
	   - Undefind
	   - Null
	   - Symbol
	   - Object
	- Javascript comparison

	   - === so sánh hai đối tượng có cùng giá trị và kiểu dữ liệu
	   - == so sánh hai đối tượng có cùng giá trị
	   - ">== " "<== "
	   - !==
	   - <=
	   - ">="
	   - <
	   - >

     -  JAVASCRIPT VARIABLES
  
		- var
		<!-- let (new in ECMAScript 6)-->  
		<!-- const (new in ECMAScript 6)-->
      - JAVASCRIPT CONDITIONALS

		- if
		- else
		- else if
		<!-- ternary operator -->
		<!-- switch -->

	- JAVASCRIPT LOGICAL OPERATORS
	
		- &&
		- ||
		- !

	- JAVASCRIPT FUNCTIONS
	
		- var a = function name() {}
		- function name() {}
		- return
		<!-- () => (new in ECMAScript 6) -->

	- JAVASCRIPT DATA STRUCTURES
	
		- Array
		- Object

	- JAVASCRIPT LOOPING
	
		- for
		- while
		- do 
		- forEach (new in ECMAScript 5) 


	- JAVASCRIPT KEYWORDS
		 - break case catch class const continue debugger default delete do else export extends finally for function if import in instanceof newreturn super switch this throw try typeof var void while with yield


