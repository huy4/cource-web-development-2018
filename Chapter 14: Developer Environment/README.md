 * chapter 14: Developer Environment
	 * Sublime text 3
		 * Customize Sublime text
		 * Change color, icon, install packet support,...
	 * Terminal
		 * Set up text color, background,... for terminal
		 * For Mac:
			 * Option 1: Terminal
			
				[Customizing Terminal](http://osxdaily.com/2013/02/05/improve-terminal-appearance-mac-os-x/)  
			  
			* Option 2: [iTerm2](https://www.iterm2.com/index.html)  
			  
				[Customizing iTerm2](http://sourabhbajaj.com/mac-setup/iTerm/README.html)

