# 11/06/2018
* Chapter 1: Introduction
* Chapter 2: How The Internet Works
    - tracer ip

# 12/06/2018
* Chapter 3: History of the Web
    - www and internet

* Chapter 4: HTML5
    - html tags
    - use name
    - relative and absolute

* chapter 5:  Advanced HTML5
    - form
    - submitting
    - div, span and more tags in html

# 15/06/2018
* Chapter 6: CSS
    - Giúp trang trí style site theo ý muốn (font, site, căn trái, phải,..)
    - Box model
    - Sử dụng google font

# 17/06/2018
* Chapter 7: CSS Advanced
    - Critical render path
    - Responsive UI
    - CSS 3:
	- transition
	- tranform
	- Flex-box
* Chapter 8: Bootstrap 4, Templates, And Building Your Startup Landing Page
    - Bootstrap xây dựng theo các lưới (Grid)
    - Các Template có sẵn (README.md trong folder Chapter 8)
    - Building landing page in Github

#26/06/2017
* Chapter 9: Career a Web developer
    - In-demand
    - Career options
    - connect developer communication: in stackoverflow, slack, discard, ...
