 * chapter 11: DOM manipulation
	 * The DOM is a W3C (World Wide Web Consortium) standard.
	 ![The HTML DOM Tree of Objects](https://www.w3schools.com/js/pic_htmltree.gif)
	 * With the object model, JavaScript gets all the power it needs to create dynamic HTML:
		 -   JavaScript can change all the HTML elements in the page
		-   JavaScript can change all the HTML attributes in the page
		-   JavaScript can change all the CSS styles in the page
		-   JavaScript can remove existing HTML elements and attributes
		-   JavaScript can add new HTML elements and attributes
		-   JavaScript can react to all existing HTML events in the page
		-   JavaScript can create new HTML events in the page
	* query selector trả về NodeList
	* get elements trả về HTMLCollection
		* Cả 2 đều cung cấp chỉ mục truy cấp giống array gồm các đối tượng
		* Các NoteList chỉ có thể truy cập bởi các chỉ mục của chúng
		* Các mục HTMLCollection có thể được truy cập theo tên, id hoặc số thứ tự của chúng
	* [JQuery](http://jquery.com)

