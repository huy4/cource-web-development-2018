# Chapter 18 React + Redux
## React
- ReactJS là một thư viện của javascript được thiết kế theo xu hướng single page application. ReactJS cho phép nhúng code html trong code javascript  thông qua [JSX](https://reactjs.org/docs/jsx-in-depth.html), điểm mạnh của JSX là tốc độ nhanh, độ an toàn cao.
- React là một thư viện UI được phát triển bởi facebook để hỗ trợ việc xây dựng các thành phần (components) UI có tính tương tác cao.
- Các components có đặc điểm là không thay đổi giá trị. Component được tạo ra bằng các gọi phương thức createClass của đối tượng React.
- Hàm **createClass** nhận vào một tham số, là đối tượng mô tả đặc tính của component. Đối tượng này bao gồm tất cả các phương thức để hình thành nên component. Phương thức quan trọng nhất là **render**, phương thức này được trigger khi component đã sẵn sàng để được render lên trên page.



## Redux

- Redux js là một thư viện Javascript giúp tạo ra thành một lớp quản lý trạng thái của ứng dụng.
- Một số khái niệm:
	- Action: là nơi chứa các thông tin dùng để gửi từ ứng dụng tới store
	- Reducer: là nơi xác định state thay đổi thế nào
	- Store: là nơi quản lý các state, truy cập bằng getState, update state qua dispatch action, đăng ký qua subcribe
	- View: hiển thị dữ liệu cung cấp từ store.
![enter image description here](https://viblo.asia/uploads/d3e12c8e-7b0b-4874-a119-1eaaf24ea165.png)


