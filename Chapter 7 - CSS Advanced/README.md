* chapter 7:  CSS Advanced
  - critical render path
  - minifying CSS
  - flex box [CSS Trick](https://css-tricks.com/snippets/css/a-guide-to-flexbox/) : flex-wrap, justify-content, align-content
  - Thực hành thêm flex box [Froggy](https://flexboxfroggy.com/)
  - Transition: biến đổi của object theo thời gian / Transform: chuyển tiếp (sang các dạng hình khác)
  - shadow
  - @keyframes
  - sử dụng position + sau đó là top/right/bottom/left
  - sử dụng tương thích cho các browser: -webkit-, -moz-, -ms-, -o-

