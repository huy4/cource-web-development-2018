# Chapter 17 Npm + Npm Script
- [Npm](https://docs.npmjs.com/) - Node package manager
- Là công cụ để quản lý việc cài đặt các package javascript cho nodejs
- Để cài đặt : [Npm install](https://docs.npmjs.com/getting-started/installing-node)
- [Local package Database](https://docs.npmjs.com/getting-started/installing-npm-packages-locally): chứa các thông tin các package trong project
- Nhược điểm của npm là để cài đặt package thì phải lần lượt từng package để khắc phục nhược điểm đó facebook và google đã đưa ra giải pháp là Yarn
- [Yarn Getting Started](https://yarnpkg.com/lang/en/docs/getting-started/)

