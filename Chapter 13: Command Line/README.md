 * chapter 13: Command Line
	 * Using the Terminal
	 * Linux & Mac
		 * ls liệt kê các file
		 * cd folder : di chuyển đến thư mục
		 * cd .. : trở về thư mục trước
		 * cd \ : trở về thư mục gốc
		 * pwd : xem đường dẫn thư mục hiện tại
		 * rm file : xóa file
		 * rm -r folder : xóa folder
		 * touch tên file : tạo tên file
		 * mkdir : tạo folder
	 * Window	
		 * dir : liệt kê các file
		 * cd : thay đổi đường dẫn
		 * cd / : về thư mục gốc
		 * cd .. : về thư mục trước
		 * mkdir {file name} - make a directory
		 * echo > {filename} - create an empty file del {filename} - remove a
		 *  file rmdir {directory name} - remove a directory and all files within
		 *  rename {filename} {new filename} - rename a file or folder start
		 *  {filename} - open file in default program start . - open current
		 * directory cls - clear the terminal screen
	 * [Comparison between Windows and Linux & Mac](https://skimfeed.com/blog/windows-command-prompt-ls-equivalent-dir/)

