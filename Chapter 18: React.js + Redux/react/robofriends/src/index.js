import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './containers/App';
// import Card from './Card';
// import CardList from './CardList';
import registerServiceWorker from './registerServiceWorker';
import 'tachyons';
// import SearchBox from './SearchBox';


ReactDOM.render(
	< App />
	, document.getElementById('root'));
registerServiceWorker();
